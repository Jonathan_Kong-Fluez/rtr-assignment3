
#define GL_GLEXT_PROTOTYPES

#define _USE_MATH_DEFINES // for C

#include <math.h>

#if defined(_WIN32) || defined(WIN32)     /* _Win32 is usually defined by compilers targeting 32 or   64 bit Windows systems */

#include <time.h>

#else

#include <sys/time.h>

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <limits.h>
#include <random>


/* Debugging controls .*/
enum debugFlags {
	debug_time,
	debug_wall,
	debug_initialise_particle,
	debug_particle,
	debug_particle_collision,
	debug_collideParticlesBruteForce,
	debug_collideParticlesUniformGrid,
	debug_collisionReactionParticles2DbasisChange,
	debug_collisionReactionParticles2DprojNormal,
	debug_framerate,
	debug_range_check,
	debug_sum_kinetic_energy,
	debug_sum_momentum,
	numDebugFlags
};
int debug[numDebugFlags] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 };

/* Use our type so we can change precision easily. */
typedef double Real;

/* Small number to handle numerical imprecision. */
const Real epsilon = 1.0e-6;

/* Ball*/
struct Ball
{
	int lives = 5;
	Real position[2];
	Real velocity[2];
	Real radius;
	Real mass;
	Real elasticity;
	GLUquadric *quadric;  /* For rendering. */
	int slices, loops;    /* For rendering. */
	int score;
};

//Catcher
struct Catcher
{
	bool colided = false;
	Real position[2];
	Real velocity[2];
	Real radius;
	Real mass;
};

/*Pegs*/
struct Peg
{
	float f;
	bool colided = false;
	bool del = false;
	Real position[2];
	Real radius;
	Real mass;
	Real elasticity;
	GLUquadric *quadric;  /* For rendering. */
	int slices, loops;    /* For rendering. */
};

/* Control 1D or 2D */
const int dimension = 2;

/* Control random or explicit initial positions */
typedef enum position
{
	randomly,
	explicitly
}position;

const position initialiseParticles = randomly;

/* Control collision reaction calculation */
typedef enum ReactionCalculation
{
	basisChange,
	projNormal
}ReactionCalculation;

ReactionCalculation reacCalc = basisChange;

/* num is pegs*/
const int NEW_NUM_OF_PEGS = 98;
/*peg array*/
Peg PEGS[NEW_NUM_OF_PEGS];

Ball player;
Catcher catcher;


/* Arena. */
struct Arena {
	Real min[2], max[2];
	Real momentum[2];
} arena;

/* Rendering info. */
enum renderMode { wire, solid };
static renderMode renMode = wire;
static Real elapsedTime = 0.0, startTime = 0.0;
static const int milli = 1000;
static bool go = false;
float x3;

/* Collision detection method. */
enum CollisionDetectionMethod {
	bruteForce,
	uniformGrid
};

CollisionDetectionMethod CDmethod = bruteForce;

/*globals*/
struct Global
{
	float gravity = 9.8;
	float speed = 1;
	bool earthG = true;

}global;

GLfloat oneColor[3] = { 0, 1, 0 };

Real random_uniform() {
	return rand() / (float)RAND_MAX;
}

void panic(const char *m) {
	printf("%s", m);
	exit(1);
}

void initialiseArena()
{
	const Real halfLength = 9.5;

	arena.min[0] = -halfLength;
	arena.min[1] = -halfLength;
	arena.max[0] = halfLength;
	arena.max[1] = halfLength;

	arena.momentum[0] = 0.0;
	arena.momentum[1] = 0.0;
}

// where you init player
void initplayerBall()
{
	GLUquadric *quadric = gluNewQuadric();

	player.radius = 1;
	player.mass = 1.0;
	player.elasticity = 1.0;
	player.quadric = quadric;
	player.slices = 10;
	player.loops = 3;
	player.score = 0;

	player.position[0] = 0;
	player.position[1] = 8;
	player.velocity[1] = 0;
	player.velocity[0] = 0;
}

// where you init peg
void initPeg()
{
	for (int i = 0; i < NEW_NUM_OF_PEGS; i++)
	{

		PEGS[i].radius = 2;
		PEGS[i].mass = 1.0;
		PEGS[i].elasticity = 1.0;
		PEGS[i].quadric = gluNewQuadric();
		PEGS[i].slices = 10;
		PEGS[i].loops = 3;
	}
	int x = 0;
	int z = 0;

	int xPos = 0, yPos = 0;
	int x1 = NEW_NUM_OF_PEGS / 2;
	float xgen;
	float ygen;

	std::random_device rseed;
	std::mt19937 gen(rseed()); //Standard mersenne_twister_engine seeded with rd()

	std::uniform_real_distribution<> disx(arena.min[0] + 0.5, arena.max[0] - 0.5);
	std::uniform_real_distribution<> disy(arena.min[1] + 3, arena.max[1] - 2);

	// generating ramdon peg position and checking if its colliding which sometimes jsut dont work~
	for (int y = 0; y < NEW_NUM_OF_PEGS; ++y)
	{
		xgen = disx(gen);
		ygen = disy(gen);

		for (int x = 0; x < y; ++x)
		{
			float tempVecx = xgen - PEGS[x].position[0];
			float tempVecy = ygen - PEGS[x].position[1];

			float n_mag = sqrtf((tempVecx * tempVecx) + (tempVecy * tempVecy));

			float tempRad = PEGS[y].radius * 2 * 0.2;

			if (n_mag < tempRad)
			{
				xgen = disx(gen);
				ygen = disy(gen);
				x = 0;
			}
		}

		PEGS[y].position[0] = xgen;
		PEGS[y].position[1] = ygen;
	}
}

// init catcher
void initCatcher()
{
	catcher.mass = 1.0;
	catcher.position[0] = 0;
	catcher.position[1] = -8;
	catcher.velocity[0] = 10;
}

void setRenderMode(renderMode rm)
{
	/* Example of GNU C/C++ brace indentation style.  */
	if (rm == wire)
	{
		glDisable(GL_LIGHTING);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_NORMALIZE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else if (rm == solid)
	{
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_NORMALIZE);
		glShadeModel(GL_SMOOTH);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void displayPegs(Peg *peg, float scaleX, float scaleY, float scaleZ)
{
	glPushMatrix();
	glScalef(scaleX, scaleY, scaleZ);
	gluDisk(peg->quadric, 0.0, peg->radius, peg->slices, peg->loops);
	glPopMatrix();
}

void displayBall(Ball *ball, float scaleX, float scaleY, float scaleZ)
{
	glColor3f(0, 0, 1);
	GLfloat color[3] = { 0 , 0 ,1 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &color[0]);

	glPushMatrix();
	glScalef(scaleX, scaleY, scaleZ);
	gluDisk(ball->quadric, 0.0, ball->radius, ball->slices, ball->loops);
	glPopMatrix();
}

void displayCatcher(Catcher *c)
{
	glColor3f(1, 1, 1);
	GLfloat color[3] = { 1 , 1 ,1 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &color[0]);

	glPushMatrix();
	glRectf(0, 0, 2, 0.5);
	glPopMatrix();
}

void changeRenderMode(void)
{
	if (renMode == wire) {
		renMode = solid;
	}
	else {
		renMode = wire;
	}
	setRenderMode(renMode);
}

void displayArena(void)
{
	GLfloat color[3] = { 1 , 1 ,1 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &color[0]);


	glBegin(GL_LINE_LOOP);
	glVertex3f(arena.min[0], arena.min[1], 0.0);
	glVertex3f(arena.max[0], arena.min[1], 0.0);
	glVertex3f(arena.max[0], arena.max[1], 0.0);
	glVertex3f(arena.min[0], arena.max[1], 0.0);
	glEnd();
}

void integrate()
{
	static float t = 0.0, h;

	//ccalculate time increment
	h = (elapsedTime - t) * global.speed;
	t = elapsedTime;

	if (h > 0.1)
	{
		h = 0.1;
	}

	player.velocity[1] -= global.gravity * h;

	// compute new position of disks
	player.position[1] += h * player.velocity[1];
	player.position[0] += h * player.velocity[0];

	catcher.position[0] += h * catcher.velocity[0];
}

//where player is deleted 
void destroy()
{
	for (int i = 0; i < NEW_NUM_OF_PEGS; ++i)
	{
		if (PEGS[i].del)
		{
			PEGS[i].radius = 0;
			PEGS[i].slices = 0;
			PEGS[i].position[0] = arena.min[0];
		}
	}
}

void collide()
{
	int i;

	float m1 = player.mass;

	//collision againts pegs
	for (i = 0; i < NEW_NUM_OF_PEGS; i++)
	{
		float xd = player.position[0] - PEGS[i].position[0];
		float yd = player.position[1] - PEGS[i].position[1];

		/* Vector projection/component/resolute of velocity in n direction. */
		float v1i = player.velocity[0] * xd + player.velocity[1] * yd;

		float sumRadius = (PEGS[i].radius / 10) + (player.radius / 4);

		float sqrRadius = sumRadius * sumRadius;

		float distSqr = (xd * xd) + (yd * yd);

		if (distSqr <= sqrRadius)
		{
			//normalise
			float n_mag = sqrt(xd * xd + yd * yd);
			xd /= n_mag;
			yd /= n_mag;

			/* Use 1D equations to calculate final velocities in n direction. */
			float m2 = PEGS[i].mass;
			float M = m1 + m2;
			float v1f = (m1 - m2) / (m1 + m2) * v1i + 2.0 * m2 / (m1 + m2) * 1;

			/* Vector addition to solve for final velocity. */
			player.velocity[0] = (player.velocity[0] - v1i * xd) + v1f * xd;
			player.velocity[1] = (player.velocity[1] - v1i * yd) + v1f * yd * 10;

			//destroy(i);
			PEGS[i].colided = true;

			// where score are added
			if (!PEGS[i].del)
				player.score++;

			PEGS[i].del = true;

			x3 = 1;
		}
	}

	if (player.position[1] - (player.radius / 4) <= catcher.position[1])
	{
		if (player.position[0] - (player.radius / 4) <= catcher.position[0] + 2 && player.position[0] + (player.radius / 4) >= catcher.position[0])
		{
			player.velocity[1] *= -1;
		}
	}

	//if collide left and right
	if (player.position[0] - (player.radius / 4) <= arena.min[0] || player.position[0] + (player.radius / 4) >= arena.max[0])
	{
		if (player.position[0] + (player.radius / 4) >= arena.max[0])
			player.position[0] = arena.max[0] - (player.radius / 4);
		else
			player.position[0] = arena.min[0] + (player.radius / 4);

		player.velocity[0] *= -1;
	}

	// if collide up and down
	if (player.position[1] + (player.radius / 4) >= arena.max[1])
	{
		player.position[1] = arena.max[1] - (player.radius / 4);
		player.velocity[1] *= -1;
	}

	if (player.position[1] + (player.radius / 4) <= arena.min[1])
	{
		go = false;

		player.position[0] = 0;
		player.position[1] = 8;
		player.velocity[1] = 0;
		player.velocity[0] = 0;

		catcher.position[0] = -8;

		destroy();
		player.lives--;
	}
}

//catcher's collider
void collideCatcher()
{
	if (catcher.position[0] + 2 >= arena.max[0] || catcher.position[0] <= arena.min[0])
	{
		catcher.velocity[0] *= -1;
	}
}

//interpolation
float lerp(float a, float b, float f)
{
	return a + f * (b - a);
}

void drawParabola()
{
	float x = player.position[0];
	float y = player.position[1];
	float vX = player.velocity[0];
	float vY = player.velocity[1];
	float dt;

	glBegin(GL_LINE_STRIP);
	glColor3f(0, 0, 1);
	GLfloat color[3] = { 0 , 0 ,1 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &color[0]);

	for (int i = 0; i < 110; ++i)
	{
		dt = 0.01 * i;
		x += vX * dt;
		y += vY * dt;
		vY -= global.gravity * dt;

		//collision againts pegs
		for (int p = 0; p < NEW_NUM_OF_PEGS; p++)
		{
			float m1 = player.mass;
			float xd = x - PEGS[p].position[0];
			float yd = y - PEGS[p].position[1];

			/* Vector projection/component/resolute of velocity in n direction. */
			float v1i = vX * xd + vY * yd;

			float sumRadius = (PEGS[p].radius / 10) + (player.radius / 4);

			float sqrRadius = sumRadius * sumRadius;

			float distSqr = (xd * xd) + (yd * yd);

			if (distSqr <= sqrRadius)
			{
				//normalise
				float n_mag = sqrt(xd * xd + yd * yd);
				xd /= n_mag;
				yd /= n_mag;

				/* Use 1D equations to calculate final velocities in n direction. */
				float m2 = PEGS[i].mass;
				float M = m1 + m2;
				float v1f = (m1 - m2) / (m1 + m2) * v1i + 2.0 * m2 / (m1 + m2) * 1;

				/* Vector addition to solve for final velocity. */
				vX = vX - v1i * xd + v1f * xd;
				vY = vY - v1i * yd + v1f * yd * 10;
			}

		}
		glVertex2f(x, y);
	}

	glColor3f(1, 1, 1);
	glEnd();
}

void displayPegs(void)
{
	GLfloat color[3] = { x3,1,0 };
	int i;
	/* Display pegs. */
	for (i = 0; i < NEW_NUM_OF_PEGS; i++)
	{
		glPushMatrix();
		glTranslatef(PEGS[i].position[0], PEGS[i].position[1], 0.0);
		/*interpolation light*/
		if (PEGS[i].colided)
		{
			glColor3fv(color);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, &color[0]);
		}
		else
		{
			glMaterialfv(GL_FRONT, GL_DIFFUSE, &oneColor[0]);
			glColor3f(0, 1, 0);
		}
		displayPegs(&PEGS[i], 0.1, 0.1, 0.1);
		glPopMatrix();

	}
}

void displayBall(void)
{
	/* Display particles. */
	glPushMatrix();
	glTranslatef(player.position[0], player.position[1], 0.0);
	displayBall(&player, 0.2, 0.2, 0.2);
	glPopMatrix();

}

void displayCatcher(void)
{
	/* Display particles. */
	glPushMatrix();
	glTranslatef(catcher.position[0], catcher.position[1], 0.0);
	displayCatcher(&catcher);
	glPopMatrix();
}

void displayOSD(int frameNo)
{
	static const Real interval = 1.0;
	static Real frameRateInterval = 0.0;
	static int frameNoStartInterval = 0;
	static Real elapsedTimeStartInterval = 0.0;
	static char buffer[80];
	int len, i;

	if (elapsedTime < interval)
		return;

	if (elapsedTime > elapsedTimeStartInterval + interval) {
		frameRateInterval = (frameNo - frameNoStartInterval) /
			(elapsedTime - elapsedTimeStartInterval);
		elapsedTimeStartInterval = elapsedTime;
		frameNoStartInterval = frameNo;
	}

	glColor3f(1, 0, 0);
	GLfloat color[3] = { 1, 0 ,0 };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &color[0]);

	if (debug[debug_framerate]) {
		printf("displayOSD: frameNo %d elapsedTime %f "
			"frameRateInterval %f\n",
			frameNo, elapsedTime, frameRateInterval);
	}

	if (player.lives % 2 == 1)
	{
		if (player.lives == 5)
			sprintf_s(buffer, " Health : 3");
		if (player.lives == 3)
			sprintf_s(buffer, " Health : 2");
		if (player.lives == 1)
			sprintf_s(buffer, " Health : 1");

		glRasterPos2f(6, 8.5);
		len = (int)strlen(buffer);

		for (i = 0; i < len; i++)
			glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, buffer[i]);
	}

	sprintf_s(buffer, "Score  : %d", player.score);
	glRasterPos2f(-8, 8.5);
	len = (int)strlen(buffer);

	for (i = 0; i < len; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, buffer[i]);

	sprintf_s(buffer, "framerate: %5d frametime: %5d",
		int(frameRateInterval),
		int(1.0 / frameRateInterval * 1000));
	glRasterPos2f(-10, -9);
	len = (int)strlen(buffer);

	for (i = 0; i < len; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, buffer[i]);

	if (player.lives == 0)
	{
		sprintf_s(buffer, "You ran out of lives!");

		glRasterPos2f(-3, -1);
		len = (int)strlen(buffer);

		for (i = 0; i < len; i++)
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, buffer[i]);
	}
}

void update(void)
{
	static float t = 0.0, h;
	//calculate time increment
	h = elapsedTime - t;
	t = elapsedTime;
	float x1 = 1;
	float x2 = 0;

	//interpolating lights lit to unlit
	for (int i = 0; i < NEW_NUM_OF_PEGS; i++)
	{
		if (PEGS[i].colided)
		{
			PEGS[i].f += h * 0.3;
			x3 = lerp(x1, x2, PEGS[i].f);
		}
		if (PEGS[i].f >= 1)
		{
			PEGS[i].f = 0;
			PEGS[i].colided = false;
		}

	}
	elapsedTime = glutGet(GLUT_ELAPSED_TIME) / (Real)milli - startTime;


	if (player.lives == 0)
	{
		printf("You have lost. \n");
		return;
	}

	if (!go)
		return;

	collideCatcher();
	collide();
	integrate();
	glutPostRedisplay();

}

void display(void)
{
	static int frameNo = 0;
	GLenum err;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f(0.8, 0.8, 0.8);

	glPushMatrix();

	/* Display particle and arena. */
	glPushMatrix();
	displayArena();
	displayPegs();
	displayBall();
	if (!go)
		drawParabola();
	displayCatcher();
	glPopMatrix();

	/* Display frame rate counter. */
	glPushMatrix();
	displayOSD(frameNo);
	glPopMatrix();
	frameNo++;

	glPopMatrix();

	glutSwapBuffers();
	/* Check for errors. */
	while ((err = glGetError()) != GL_NO_ERROR)
		printf("%s\n", gluErrorString(err));
}

void myInit(void)
{
	setRenderMode(renMode);
	initialiseArena();
	initplayerBall();
	initPeg();
	initCatcher();
}

void keyboardCB(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27: //ESC
	case 'q':
		exit(EXIT_SUCCESS);
		break;
	case 'w':
		changeRenderMode();
		break;
	case 'c':
		debug[debug_range_check] = !debug[debug_range_check];
		break;
	case 'd':
		if (CDmethod == uniformGrid)
			CDmethod = bruteForce;
		else if (CDmethod == bruteForce)
			CDmethod = uniformGrid;
		break;
	case 'r':
		if (reacCalc == projNormal)
			reacCalc = basisChange;
		else if (reacCalc == basisChange)
			reacCalc = projNormal;
		break;
	case ' ':
		if (!go)
		{
			startTime = glutGet(GLUT_ELAPSED_TIME) / (Real)milli;
			go = true;
		}
		break;

	case '+':
		global.speed += 0.1;
		break;

	case '-':
		if (global.speed >= 0.5)
		{
			global.speed -= 0.1;
			global.speed -= 0.1;
		}
		break;

	case 'g':
		if (global.earthG)
		{
			global.gravity = 9.8;
			global.earthG = !global.earthG;
		}
		else
		{
			global.gravity = 1.622;
			global.earthG = !global.earthG;
		}
		break;

	default:
		break;
	}
	glutPostRedisplay();
}

void specialKeys(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_LEFT:
		if (!go)
		{
			player.velocity[0] -= 0.1;
			printf("Player Velocity :  %2f\n", player.velocity[0]);
		}
		break;

	case GLUT_KEY_RIGHT:
		if (!go)
		{
			player.velocity[0] += 0.1;
			printf("Player Velocity :  %2f\n", player.velocity[0]);
		}
		break;

	default:
		break;
	}
	glutPostRedisplay();
}

void myReshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-10.0, 10.0, -10.0, 10.0, -10.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/*  Main Loop
*  Open window with initial window size, title bar,
*  RGBA display mode, and handle input events.
*/
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(800, 300);
	glutCreateWindow("Peggle Assignment 3");
	glutDisplayFunc(display);
	glutIdleFunc(update);
	glutReshapeFunc(myReshape);
	glutKeyboardFunc(keyboardCB);
	glutSpecialFunc(specialKeys);
	myInit();
	glutMainLoop();
}